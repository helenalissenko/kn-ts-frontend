# kn-ts-frontend
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## To run the application:
  1. run `npm install`
  2. run `npm run start`
  3. open [http://localhost:3000](http://localhost:3000) in your browser

The application was tested in Chrome, Firefox and Internet Explorer. For styling Google Material Design was used.

# Requirements
## Login screen
The application is behind a login screen, without logging in you are unable to access any of the routes. There is no need to register, just enter some kind of username and some kind of password. Username is persisted until you reload the page, the you are presented with login form again, password is not kept.

## History of previously uploaded files
After logging in you see the home page which also has the list of recently uploaded locations, by default it is empty. When you upload a couple of JSON files, you will see them on the page in a list. Clicking on the title will expand a map with all the locations from the file shown as pins. If it was you who uploaded the file, you will also see an Edit button, which takes you to "entity" page where you can add more locations.

## Uploading a JSON file
From the menu you can access the Upload page. The structure of the JSON file is the following:
```
{
    "locations": [
        {
            "lat": 23.1136,
            "long": -82.3666
        },
        {
            "lat": 35.6895,
            "long": 139.6917
        }
    ]
}
```
There are two example JSON files in the *kn-node-backend* project named `example.json` and `example2.json`

## Ability to add new locations to an existing uploaded data set
After uploading the file you can access the entity page from the Edit button. On the entity page you have the button to add a new location to the set. Clicking the button shows a form for coordinates, after clicking Add button, you can also see the new set of coordinates in the list.

## Separately selectable locations and my current location
Clicking Show on map button displays the selected location as a pin on the map. The map also shows my current location (need to allow location from browser), the current location display is delayed by 3 seconds in the code, so it does not show up right away.

## Ability to download a selection of locations as JSON file
On the entity page you can select one or more locations with checkboxes and when you click Download a JSON file with those locations is downloaded. The format is the same as the uploaded file.

## Screenshot of the view for sharing
Not implemented

## E2E testing framework
Nightwatchjs is set up for this project. There is only two tests present for the login page and actual logging in action. To run the test, run the `nightwatch` command in the command line. Both the backend and frontend projects need to be running for this.

## Nginx inside Docker container
Not done
