module.exports = {
  beforeEach: browser => {
    browser
      .url('http://localhost:3000')
      .waitForElementVisible('body', 1000)
      .waitForElementVisible('#root > .login-form', 1000).saveScreenshot('reports/homepage.png');
  },
  'Validate Login form and title of the page': browser => {
    browser
      .assert.visible('#root > .login-form', 'Check if app has rendered with React')
      .assert.title('Mapify');
  },
  'Login into the application': browser => {
    browser
      .setValue('input[type=text]', 'nightwatch')
      .setValue('input[type=password]', 'password')
      .click('button[name=login]')
      .pause(1000)
      .assert.containsText('#root', 'Welcome to Mapify');
  },
  after: browser => browser.end(),
};