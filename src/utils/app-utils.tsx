import * as constants from '../constants/app-constants';

export const getUploadUrl = () => constants.BACKEND_URL + constants.UPLOAD_PATH;
export const getAllUploadsUrl = () => constants.BACKEND_URL + constants.UPLOADS_LIST_PATH;
export const getSaveUsernameUrl = () => constants.BACKEND_URL + constants.SAVE_USERNAME_PATH;
export const getEntityUrl = () => constants.BACKEND_URL + constants.ENTITY_PATH;
export const getUpdateUrl = () => constants.BACKEND_URL + constants.UPDATE_LOCATION_PATH;