import CssBaseline from '@material-ui/core/CssBaseline';
import * as React from 'react';
import { withRouter } from 'react-router-dom';
import qs from 'qs';

import LandingPage from './pages/landing-page';
import AuthPage from './pages/login-page';
import { getSaveUsernameUrl } from '../utils/app-utils';

import '../assets/css/style.css';

class App extends React.Component<{ history: any }, { email: string }> {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
    };
  }

  public handleChange = (email) => {
    fetch(getSaveUsernameUrl(), {
      method: 'POST',
      body: qs.stringify({ username: email }),
      headers: {
        'Accepts': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      mode: 'no-cors',
    })
      .then(response => response.ok)
      .then(() => {
        this.setState({ email });
        this.props.history.push('/home')
      });
  };

  logOut = () => {
    this.setState({ email: '' });
  }

  public render() {
    let page = <AuthPage login={this.handleChange} />;
    if (this.state.email !== "") {
      page = <LandingPage username={this.state.email} history={this.props.history} logout={this.logOut} />;
    }
    return (
      <React.Fragment>
        <CssBaseline />
        {page}
      </React.Fragment>
    );
  }
}

export default withRouter(App);
