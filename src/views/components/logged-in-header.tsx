import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import AccountCircle from '@material-ui/icons/AccountCircle';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import CloudUpload from '@material-ui/icons/CloudUpload';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Home from '@material-ui/icons/Home';
import MenuIcon from '@material-ui/icons/Menu';

import * as React from 'react';
import { Link } from 'react-router-dom';

import { classes } from '../../styles/header-style';

class LoggedInHeader extends React.Component<{ classes: any, drawerOpen: boolean, toggleDrawer: any, logout: any }, {}> {
    public render() {
        const { classes, drawerOpen, toggleDrawer } = this.props;
        return (
            <React.Fragment>
                <AppBar className={classNames(classes.appBar, {
                    [classes.appBarShift]: drawerOpen,
                    [classes[`appBarShift-left`]]: drawerOpen,
                })}>
                    <Toolbar disableGutters={!drawerOpen}>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={toggleDrawer}
                            className={classNames(classes.menuButton, drawerOpen && classes.hide)}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" noWrap={true}>
                            Welcome to Mapify
                        </Typography>
                        <AccountCircle style={{ right: 23, position: 'absolute' }} />
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="persistent"
                    open={drawerOpen}
                    anchor="left"
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={toggleDrawer}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                        <Link to="/home" style={{ textDecoration: 'none' }}>
                            <ListItem button>
                                <ListItemIcon>
                                    <Home />
                                </ListItemIcon>
                                <ListItemText primary="Home" />
                            </ListItem>
                        </Link>
                        <Link to="/upload" style={{ textDecoration: 'none' }}>
                            <ListItem button>
                                <ListItemIcon>
                                    <CloudUpload />
                                </ListItemIcon>
                                <ListItemText primary="Upload" />
                            </ListItem>
                        </Link>
                    </List>
                    <Divider />
                    <List>
                        <ListItem button onClick={() => this.props.logout()}>
                            <ListItemIcon>
                                <ExitToApp />
                            </ListItemIcon>
                            <ListItemText primary="Sign out" />
                        </ListItem>
                    </List>
                </Drawer>
            </React.Fragment>
        );
    }
}

export default withStyles(classes)(LoggedInHeader);