import * as React from 'react';
import { map, isEmpty, filter } from 'lodash';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

class MapComponent extends React.Component<{ locations: any, showCurrentLocation: boolean, currentLatLng: any }, {}> {
    render() {
        const { locations, showCurrentLocation, currentLatLng } = this.props;
        const positions = filter(locations, (location) => !isEmpty(location));
        const mapContent = (
            <GoogleMap
                defaultZoom={1}
                defaultCenter={{ lat: 0, lng: 0 }}
            >
                {
                    map(positions, (location, index) => <Marker key={index} position={{ lat: location.lat, lng: location.long }} />)
                }
                {
                    showCurrentLocation &&
                    <Marker position={{ lat: currentLatLng.lat, lng: currentLatLng.lng }} />
                }
            </GoogleMap>
        )

        return (
            <React.Fragment>
                {mapContent}
            </React.Fragment>
        );
    }
}

export default withScriptjs(withGoogleMap(MapComponent));