import * as React from 'react';
import { BrowserRouter as Link, Router, Switch, Route } from 'react-router-dom';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import EntityPage from './entity-page';
import HomePage from './homepage';
import FileUpload from './file-upload';
import LoggedInHeader from '../components/logged-in-header';

import { classes } from '../../styles/header-style';

class LandingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            drawerOpen: false,
        };
    }

    toggleDrawer = () => this.setState({ drawerOpen: !this.state.drawerOpen });

    render() {
        const { classes, username, history } = this.props;
        return (
            <div className={classes.appFrame}>
                <LoggedInHeader
                    drawerOpen={this.state.drawerOpen}
                    toggleDrawer={this.toggleDrawer}
                    logout={this.props.logout}
                    username={username}
                />
                <main
                    className={classNames(classes.content, classes[`content-left`], {
                        [classes.contentShift]: this.state.drawerOpen,
                        [classes[`contentShift-left`]]: this.state.drawerOpen,
                    })}
                >
                    <div className={classes.drawerHeader} />
                    <Switch>
                        <Route path="/home" render={props => (
                            <HomePage {...props} username={username} history={history} />
                        )} />
                        <Route path='/upload' render={() => <FileUpload username={username} />} />
                        <Route path='/uploads/:uploadName' component={EntityPage} />
                    </Switch>
                </main>
            </div>
        );
    }
}

export default withStyles(classes)(LandingPage);
