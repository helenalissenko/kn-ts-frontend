import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { cloneDeep } from 'lodash';

import { getAllUploadsUrl } from '../../utils/app-utils';
import RecentUploads from './recent-uploads';
class HomePage extends React.Component<{ username: string, history: any }, { recentFiles: { [index: string]: any } }> {
    constructor(props: any) {
        super(props);
        this.state = {
            recentFiles: [],
        };
    }

    componentDidMount() {
        fetch(getAllUploadsUrl())
            .then(response => response.json())
            .then(data => this.setState({ recentFiles: cloneDeep(data) }));
    }

    public render() {
        const { recentFiles } = this.state;
        const { username, history } = this.props;
        return (
            <div>
                <Typography variant="title" color="inherit">
                    Recently uploaded files
                </Typography>
                <RecentUploads recentFiles={recentFiles} username={username} history={history} />
            </div>
        );
    }
}

export default HomePage;
