import * as React from 'react';
import { map, isEmpty, get } from 'lodash';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import UploadedItem from './uploaded-item';

import { styles } from '../../../styles/recent-uploads-style';
class RecentUploads extends React.Component<{ recentFiles: { [index: number]: any }, classes: any, username: string, history: any }, { expanded: any }> {
    constructor(props) {
        super(props);
        this.state = {
            expanded: null,
        };
    }

    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    render() {
        const { classes, recentFiles, username, history } = this.props;
        const { expanded } = this.state;
        const uploads = get(recentFiles, 'uploads', []);
        let files = <Typography >No uploads yet</Typography>
        if (!isEmpty(uploads)) {
            files = (
                <React.Fragment>
                    {map(uploads, (file, index) => (
                        <UploadedItem
                            history={history}
                            username={username}
                            key={index} index={index}
                            expanded={expanded}
                            handleChange={this.handleChange}
                            file={file}
                        />))}
                </React.Fragment>
            );
        }
        return (
            <div className={classes.root}>
                {files}
            </div>
        );
    }
}

export default withStyles(styles)(RecentUploads);
