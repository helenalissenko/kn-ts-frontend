import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Edit from '@material-ui/icons/Edit';
import { isEmpty } from 'lodash';
import MapComponent from '../../components/map';
import { Link } from 'react-router-dom';

import { styles } from '../../../styles/recent-uploads-style';
class UploadedItem extends React.Component<{ classes: any, index: number, handleChange: any, expanded: any, file: any, username: string, history: any }, {}> {
    constructor(props) {
        super(props);
    }

    goToEntityPage = (param) => this.props.history.push(`/item/${param}`);

    render() {
        const { classes, index, handleChange, expanded, username } = this.props;
        const { user, fileName, contents } = this.props.file;
        const coordinates = isEmpty(contents) ? contents : JSON.parse(contents).locations;
        const parts = fileName.split(".");
        const param = parts[0];
        return (
            <ExpansionPanel expanded={expanded === `panel${index}`} onChange={handleChange(`panel${index}`)}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>{fileName}</Typography>
                    <Typography className={classes.secondaryHeading}>Uploaded by: {user}</Typography>
                    {user === username && <Link to={`/uploads/${param}`} style={{ right: 23, position: 'absolute' }}><Edit />Edit</Link>}
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <MapComponent
                        locations={coordinates}
                        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0BvyuGnkOrim77HFdWK6RP3gml7t41Y8&v=3.exp&libraries=geometry,drawing,places"
                        loadingElement={<div style={{ height: `100%`, width: '100%' }} />}
                        containerElement={<div style={{ height: `400px`, width: '100%' }} />}
                        mapElement={<div style={{ height: `100%` }} />}
                        showCurrentLocation={false}
                        currentLatLng={{}}
                    />
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
}

export default withStyles(styles)(UploadedItem);
