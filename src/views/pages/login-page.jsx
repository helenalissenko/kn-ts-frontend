import * as React from 'react';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import get from 'lodash/get';

const classes = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
});

class AuthPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {},
        };
    }

    handleChange = ({ target }) => this.setState({ value: { ...this.state.value, [target.name]: target.value } });

    handleSubmit = () => this.props.login(get(this.state.value, 'identifier', ''));

    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <div className="login-form">
                    <hgroup>
                        <Typography variant="title" color="inherit">
                            Login
                        </Typography>
                    </hgroup>
                    <ValidatorForm ref="form" onSubmit={this.handleSubmit}>
                        <div className="group">
                            <TextValidator
                                autoFocus
                                label="Username"
                                key="identifier"
                                name="identifier"
                                onChange={this.handleChange}
                                type="text"
                                value={get(this.state.value, 'identifier', '')}
                                className={`${classes.textField} username-field`}
                                validators={['required', 'minStringLength:3', 'maxStringLength:255']}
                                errorMessages={['this field is required', 'username must be at least 3 characters long', 'username is too long']}
                            />
                        </div>
                        <div className="group">
                            <TextValidator
                                key="password"
                                name="password"
                                label="Password"
                                onChange={this.handleChange}
                                type="password"
                                value={get(this.state.value, 'password', '')}
                                className={`${classes.textField} password-field`}
                                validators={['required', 'minStringLength:6', 'maxStringLength:50']}
                                errorMessages={['this field is required', 'password must be at least 6 characters long', 'password is too long']}
                            />
                        </div>
                        <Button name="login" variant="outlined" color="primary" type="submit" disabled={get(this.state.value, 'password', '') === '' || get(this.state.value, 'identifier', '') === ''} >Login</Button>
                    </ValidatorForm>
                </div>
            </React.Fragment>

        );
    }
}

export default withStyles(classes)(AuthPage);
