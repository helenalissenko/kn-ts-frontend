import * as React from 'react';
import { getUploadUrl } from '../../../utils/app-utils';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        margin: theme.spacing.unit * 2,
    }
});

class FileUpload extends React.Component<{ username: string, classes: any }, { showSnackBar: boolean, file: string, message: string }> {
    constructor(props: any) {
        super(props);
        this.state = {
            file: '',
            showSnackBar: false,
            message: '',
        };
    }

    handleImageChange = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file,
            });
        }

        reader.readAsDataURL(file);
    }

    handleClose = () => {
        this.setState({ showSnackBar: false });
    };

    uploadFile = (e) => {
        e.preventDefault();
        let formData = new FormData()
        formData.append('jsonUpload', this.state.file);
        fetch(getUploadUrl(), {
            method: 'POST',
            body: formData,
        }).then(response => {
            if (response.status !== 200) {
                return Promise.reject({ ...response, errorMessage: `Status ${response.status}, ${response.statusText}` });
            }
            this.setState({ message: 'Uploaded successfully!', showSnackBar: true });
            return Promise.resolve({ ...response, errorMessage: `Status ${response.status}, ${response.statusText}` });
        }).catch(() => {
            this.setState({ message: 'Something went wrong', showSnackBar: true });
        });
    }

    public render() {
        const { classes } = this.props;
        return (
            <Paper className={classes.paper}>
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    open={this.state.showSnackBar}
                    onClose={this.handleClose}
                    message={<span id="message-id">{this.state.message}</span>}
                />
                <form>
                    <input type="file" name="jsonUpload" onChange={(e) => this.handleImageChange(e)} />
                    <Button variant="outlined" color="primary" type="submit" disabled={!this.state.file} onClick={(e) => this.uploadFile(e)}> Upload </Button>
                </form>
            </Paper>
        );
    }
}

export default withStyles(styles)(FileUpload);
