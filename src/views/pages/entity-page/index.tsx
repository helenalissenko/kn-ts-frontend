import * as React from 'react';
import qs from 'qs';
import { cloneDeep, get, map, isEmpty, forEach } from 'lodash';
import { getEntityUrl } from '../../../utils/app-utils';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import TableHead from '@material-ui/core/TableHead';
import Place from '@material-ui/icons/Place';
import CloudDownload from '@material-ui/icons/CloudDownload';
import DownloadLink from "react-download-link";
import FormControl from '@material-ui/core/FormControl';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import MapComponent from '../../components/map';
import { styles } from '../../../styles/table-styles';
import { getUpdateUrl } from '../../../utils/app-utils';
import Snackbar from '@material-ui/core/Snackbar';

ValidatorForm.addValidationRule('isValidNumber', (value) => {
    if (isNaN(value)) {
        return false;
    }
    return true;
});

class EntityPage extends React.Component<{ classes: any }, { message: string, value: object, showSnackBar: boolean, showAddForm: boolean, locationsForDownload: any, selectedLocations: Array<any>, entity: any, markerDown: object, currentLatLng: object, showCurrentLocation: boolean }> {
    constructor(props: any) {
        super(props);
        this.state = {
            markerDown: {},
            entity: {},
            currentLatLng: {
                lat: 0,
                lng: 0
            },
            showCurrentLocation: false,
            selectedLocations: [],
            locationsForDownload: {
                locations: [],
            },
            showAddForm: false,
            value: {},
            showSnackBar: false,
            message: '',
        };
    }

    componentDidMount() {
        this.load();
        this.delayedShowMarker();
    }

    componentWillUpdate() {
        this.getGeoLocation()
    }

    load() {
        const pathParam = window.location.pathname.split('/')[2];
        fetch(`${getEntityUrl()}/${pathParam}`)
            .then(response => response.json())
            .then(data => this.setState({ entity: cloneDeep(data) }));
    }

    delayedShowMarker = () => {
        setTimeout(() => {
            this.getGeoLocation()
            this.setState({ showCurrentLocation: true })
        }, 3000)
    }

    getGeoLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => {
                    this.setState(prevState => ({
                        currentLatLng: {
                            ...prevState.currentLatLng,
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        }
                    }))
                }
            )
        } else {
            error => console.log(error)
        }
    }

    toggleSelection(index) {
        const currentSelection = [...this.state.selectedLocations];
        if (currentSelection.indexOf(index) > -1) {
            const actualIndex = currentSelection.indexOf(index);
            if (actualIndex > -1) {
                currentSelection.splice(actualIndex, 1);
            }
        } else {
            currentSelection.push(index);
        }
        const downloads: any = [];
        const contents = get(this.state.entity, 'upload.contents', '{}');
        const locationsJson = JSON.parse(contents);
        const locations = get(locationsJson, 'locations', []);
        forEach(currentSelection, (index) => {
            downloads.push(locations[index]);
        });
        this.setState({ selectedLocations: currentSelection, locationsForDownload: { locations: downloads } });
    }

    showOnMap(lat, long) {
        this.setState({ markerDown: { lat, long } });
    }

    handleChange = ({ target }) => this.setState({ value: { ...this.state.value, [target.name]: target.value } });

    handleButtonClick = () => this.setState({ showAddForm: true });

    handleSubmit = () => {
        fetch(`${getUpdateUrl()}/${get(this.state.entity, 'upload.fileName', '')}`, {
            method: 'POST',
            body: qs.stringify({ long: get(this.state.value, 'longitude', ''), lat: get(this.state.value, 'latitude', '') }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(response => {
            if (response.status !== 200) {
                return Promise.reject({ ...response, errorMessage: `Status ${response.status}, ${response.statusText}` });
            }
            this.setState({ message: 'Added successfully!', showSnackBar: true, showAddForm: false });
            this.load();
            return Promise.resolve({ ...response, errorMessage: `Status ${response.status}, ${response.statusText}` });
        }).catch(() => {
            this.setState({ message: 'Something went wrong', showSnackBar: true });
        });
    };

    handleClose = () => {
        this.setState({ showSnackBar: false });
    };

    public render() {
        const { classes } = this.props;
        const { showCurrentLocation, currentLatLng } = this.state;
        const contents = get(this.state.entity, 'upload.contents', '{}');
        const locationsJson = JSON.parse(contents);
        const locations = get(locationsJson, 'locations', []);
        return (
            <React.Fragment>
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    open={this.state.showSnackBar}
                    onClose={this.handleClose}
                    message={<span id="message-id">{this.state.message}</span>}
                />
                <Button onClick={() => this.handleButtonClick()} color="primary" variant="outlined" className={classes.button}>Add a location to the set</Button>
                {
                    this.state.showAddForm &&
                    <ValidatorForm ref="form" onSubmit={this.handleSubmit}>
                        <FormControl className={classes.formControl}>
                            <TextValidator
                                label="Latitude"
                                autoFocus
                                key="latitude"
                                name="latitude"
                                onChange={this.handleChange}
                                type="text"
                                value={get(this.state.value, 'latitude', '')}
                                className={classes.textField}
                                validators={['required', 'isValidNumber']}
                                errorMessages={['this field is required', 'must be a number']}
                            />
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <TextValidator
                                label="Longitude"
                                key="longitude"
                                name="longitude"
                                onChange={this.handleChange}
                                type="text"
                                value={get(this.state.value, 'longitude', '')}
                                className={classes.textField}
                                validators={['required', 'isValidNumber']}
                                errorMessages={['this field is required', 'must be a number']}
                            />
                        </FormControl>
                        <Button disabled={get(this.state.value, 'latitude', '') === '' || get(this.state.value, 'longitude', '') === ''} onClick={() => this.handleSubmit()} color="primary" variant="outlined" className={classes.button}>Add</Button>
                    </ValidatorForm>
                }
                <Paper className={classes.root}>
                    <MapComponent
                        locations={[this.state.markerDown]}
                        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0BvyuGnkOrim77HFdWK6RP3gml7t41Y8&v=3.exp&libraries=geometry,drawing,places"
                        loadingElement={<div style={{ height: `100%`, width: '100%' }} />}
                        containerElement={<div style={{ height: `400px`, width: '100%' }} />}
                        mapElement={<div style={{ height: `100%` }} />}
                        currentLatLng={currentLatLng}
                        showCurrentLocation={showCurrentLocation}
                    />
                    <div className={classes.tableWrapper}>
                        <Table className={classes.table} aria-labelledby="tableTitle">
                            <TableHead>
                                <TableRow>
                                    <TableCell padding="checkbox">
                                        {!isEmpty(this.state.selectedLocations) &&
                                            <DownloadLink
                                                filename="download.json"
                                                exportFile={() => JSON.stringify(this.state.locationsForDownload)}
                                                label={<span><CloudDownload />Download</span>}
                                            />
                                        }
                                    </TableCell>
                                    <TableCell numeric>Latitude</TableCell>
                                    <TableCell numeric>Longitude</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {map(locations, (n, index) => {
                                    const isSelected = this.state.selectedLocations.indexOf(index) > -1;
                                    return (
                                        <TableRow
                                            hover
                                            role="checkbox"
                                            aria-checked={isSelected}
                                            tabIndex={-1}
                                            key={index}
                                            selected={isSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox onClick={() => this.toggleSelection(index)} checked={isSelected} />
                                            </TableCell>
                                            <TableCell numeric>{n.lat}</TableCell>
                                            <TableCell numeric>{n.long}</TableCell>
                                            <TableCell><span onClick={() => this.showOnMap(n.lat, n.long)}><Place />Show on map</span></TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </div>
                </Paper>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(EntityPage);
