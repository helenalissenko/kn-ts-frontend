export const BACKEND_URL = 'http://localhost:3030';
export const UPLOAD_PATH = '/upload';
export const UPLOADS_LIST_PATH = '/uploads';
export const SAVE_USERNAME_PATH = '/saveUsername';
export const ENTITY_PATH = '/uploads';
export const UPDATE_LOCATION_PATH = '/update';