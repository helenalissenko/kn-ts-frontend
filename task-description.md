# Constraints
## Technologies
To complete the tasks please use

* React with Typescript only
* Google Material
* NodeJS, Java or Ruby for any backend implementation necessary
* Please use an E2E testing framework or your liking. For instance we use http://nightwatchjs.org/ atm.
* Optional: The frontend has to be served by an Nginx inside a Docker container
 
## Time
After receiving this email you have seven calendar days to send your solution back to us
 
## Values
* We value expressive and clean code. If in doubt, the gist of this book: https://www.goodreads.com/book/show/3735293-clean-code
* UX can be subjectively good but also objectively bad. 
We value frontend development focusing on good UX. Please be prepared to explain your design decisions in that regard. I do remember you talking about your not too wide-ranging UX experience today. But I also got you’re a smart person… :)
 
# The Stories
 
### I as a User 
want to be able to upload a JSON document containing multiple location data sets to a web application, to have the contained locations separately selectable and displayed on a map in relation to my own location for determining the shortest route to my selected location.
Features

* A login screen is required for the application guarding the upload and all other features.
* You can make up your own JSON document format containing the location data. We only specify the JSON as: A data-set containing one or many uniquely identifiable location information. If its names, addresses or GPS coordinates or anything else is up to you.
* A history of previously uploaded location sets shall be offered by the frontend. It has to show who uploaded who uploaded each set. Clicking on an item in that list shows the locations contained in that set on a map.
* So you might need to write a simple backend for that offering a REST API to hold those uploads. The backend does not have to be fancy and may hold uploaded data only in-memory.
* There is no requirement to calculate an actual route between the users current position and one of the uploaded locations. It would be enough to display both locations on a map using some kind of pin. However if you want to build it, be my guest.
* An ability to add new locations to an existing uploaded data set through one or more input fields.
* An ability to download a selection of locations as JSON file to be able to upload this location into another application.
* Additionally, as a user I want to be able to send a screenshot of the current view, with a map, location data and link to a map view via email to my desired contact. 